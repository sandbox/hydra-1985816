<?php

$plugin = array(
  'name' => t('Public'),
  'description' => t('Adds public access.'),
  'class' => 'entity_field_access_access_type_public',
);

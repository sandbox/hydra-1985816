<?php

/**
 * TODO: Documentation.
 */
class entity_field_access_access_type_public implements entity_field_access_access_type_interface {

  function field_settings() {
    return array(
      'public' => t('Public'),
    );
  }

  function entity_settings($field_settings, $account) {
    return $this->field_settings();
  }

  function access($entity, $entity_settings, $account) {
    if ($entity_settings['public'] === TRUE) {
      return TRUE;
    }
    return NULL;
  }

  function weight() {
    return -10;
  }

}

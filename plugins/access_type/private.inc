<?php

$plugin = array(
  'name' => t('Private'),
  'description' => t('Adds privacy access.'),
  'class' => 'entity_field_access_access_type_private',
);

<?php

/**
 * TODO: Documentation.
 */
class entity_field_access_access_type_private implements entity_field_access_access_type_interface {

  function field_settings() {
    return array(
      'private' => t('Private'),
    );
  }

  function entity_settings($field_settings, $account) {
    return $this->field_settings();
  }

  function access($entity, $entity_settings, $account) {
    if ($account->uid == $entity->uid || user_access('access all private fields', $account)) {
      return TRUE;
    }
    return FALSE;
  }

  function weight() {
    return 10;
  }

}

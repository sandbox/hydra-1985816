Entity field access adds a configurable access check to any entity field.

Possible access types could be "private" or "public", for example. This allows us to set fields to private access, so that only those with the appropriate permission can view the field.

Field access for each field is configurable from within the user interface.

There is the possibility to extend the module through plugins, in order to provide other kinds of field access checks - see for example the submodules entity_field_access_role and entity_field_access_user_relationships.

<h3>Instructions</h3>

Install and enable the module as usual.

Add or edit a field as usual.

In the field edit form you will see a section entitled ENTITY PRIVATE FIELD SETTINGS.

<img src="https://drupal.org/files/project-images/field-edit-form_0.png" alt="entity_field_access field settings" />

Note: In order to provide private access, you will need to configure the "Access all private fields" permission at admin/people/permissions#module-entity_field_access.

<img src="https://drupal.org/files/project-images/permissions_0.png" alt="entity_field_access permissions" />

The following screenshot provides an example of how the settings would appear on a field on the Create Article page.

<img src="https://drupal.org/files/project-images/article-edit-form.png" alt="entity_field_access entity edit form example" />

<h3>Submodules</h3>

This module is extended by the following sub-modules:

<h4>entity_field_access_role</h4>

This allows you to restrict field access to particular roles.

<h4>entity_field_access_user_relationships</h4>

This allows you to restrict field access to users who have a particular user relationship with the node author.

This functionality depends upon the <a href="https://drupal.org/project/user_relationships">user_relationship</a> module.

The User Relationships module allows you to create relationships - for example, to allow a friendship relationship so that one user can be the "friend" of another user.

This sub-module allows you to create a restricted-access field, such that for example, only "friends" of the author can see that field.


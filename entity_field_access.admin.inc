<?php

/**
 * Implements hook_form_FORM_ID_alter().
 *
 * We alter the fields_form_field_ui edit form, to get our settings into the
 * field instance.
 */
function _entity_field_access_form_field_ui_field_edit_form_alter(&$form, &$form_state) {

  // Catch default values from the form instance.
  $default_values = isset($form['#instance']['settings']['entity_field_access']) ? $form['#instance']['settings']['entity_field_access'] : FALSE;

  $form['instance']['settings']['entity_field_access'] = array(
    '#type' => 'fieldset',
    '#title' => t('Entity Private field Settings'),
    '#description' => t('Select the available privacy settings that should be configurable on the entity edit form for this field.'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $form['instance']['settings']['entity_field_access']['type'] = array(
    '#type' => 'checkbox',
    '#title' => t('Single selection (radiobuttons)'),
    '#default_value' => isset($default_values['type']) ? $default_values['type'] : TRUE,
    '#description' => t('Select whether the selection should be single or multiple. Default is multiple (using checkboxes).')
  );

  // Generate checkboxes, dependent of the access_type field_settings definitions.
  ctools_include('plugins');
  foreach (ctools_get_plugins('entity_field_access', 'access_type') as $plugin_name => $plugin_definitions) {
    $plugin_class_name = ctools_plugin_load_class('entity_field_access', 'access_type', $plugin_name, 'class');

    $plugin = new $plugin_class_name();
    $field_settings = $plugin->field_settings();

    $form['instance']['settings']['entity_field_access']['plugins'][$plugin_name] = array(
      '#type' => 'fieldset',
      '#title' => t($plugin_definitions['name']),
      '#description' => t($plugin_definitions['description']),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    );

    foreach ($field_settings as $key => $value) {
      $form['instance']['settings']['entity_field_access']['plugins'][$plugin_name][$key] = array(
        '#type' => 'checkbox',
        '#title' => t($value),
        '#default_value' => isset($default_values['plugins'][$plugin_name][$key]) ? $default_values['plugins'][$plugin_name][$key] : FALSE,
      );
    }
  }

}


<?php

/**
 * TODO: Documentation.
 */
class entity_field_access_access_type_boolean implements entity_field_access_access_type_interface {

  function field_settings() {
    $booleans = array(
      0 => t('No'),
      1 => t('Yes'),
    );
    return $booleans;
  }

  function entity_settings($field_settings, $account) {
    return $this->field_settings();
  }

  function access($entity, $entity_settings, $account) {
    if (isset($entity_settings[0]) && $entity_settings[0] == 0) {
      return FALSE;
    }
    return TRUE;
  }

  function weight() {
    return 0;
  }

}

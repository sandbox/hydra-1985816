<?php

$plugin = array(
  'name' => t('Boolean'),
  'description' => t('Adds simple boolean (yes/no).'),
  'class' => 'entity_field_access_access_type_boolean',
);

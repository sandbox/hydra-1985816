<?php

/**
 * TODO: Documentation.
 */
interface entity_field_access_access_type_interface {
  function field_settings();

  function entity_settings($field_settings, $account);

  function access($entity, $entity_settings, $account);

  function weight();
}

<?php

/**
 * TODO: Documentation.
 */
class entity_field_access_storage {

  public static function save($entity_type, $entity_id, $private_fields_data) {
    db_merge('entity_field_access_entity_settings')
      ->key(array('entity_type' => $entity_type, 'entity_id' => $entity_id))
      ->fields(array(
        'entity_id' => $entity_id,
        'entity_type' => $entity_type,
        'entity_settings' => serialize($private_fields_data),
      ))
      ->execute();
      // Update the cache.
      // cache_clear_all('entity_field_access_entity_settings', 'cache', TRUE);
  }

  public static function load($entity_type, $entity_id) {
    // Try to load the data from cache.
    // TODO: Think about storing the data in a custom cache table.
    $entity_field_access_entity_settings = &drupal_static(__FUNCTION__);
    if(!isset($entity_field_access_entity_settings)) {
      // if ($cache = cache_get('entity_field_access_entity_settings')) {
      //   $entity_field_access_entity_settings = $cache->data;
      // }
      // else {
        $entity_field_access_entity_settings = db_select('entity_field_access_entity_settings')
          ->fields('entity_field_access_entity_settings', array('entity_settings'))
          ->condition('entity_type', $entity_type)
          ->condition('entity_id', $entity_id)
          ->execute()
          ->fetchField();
      //   cache_set('entity_field_access_entity_settings', $entity_field_access_entity_settings, 'cache');
      // }
    }

    if(!empty($entity_field_access_entity_settings)) {
      return unserialize($entity_field_access_entity_settings);
    }
    return array();
  }

  public static function delete($type, $id) {
    db_delete('entity_field_access_entity_settings')->condition('entity_type', $type)->condition('entity_id', $id)->execute();
  }

  public function fetchPrivateFieldsData($entity_type, $entity, $form, &$form_state) {
    $data = array();
    $new_data = array();

    // There might be entity types without any fields.
    $fields = isset($form_state['field']) ? $form_state['field'] : array();

    // A hack to deal with profile2 fields.
    if (module_exists('profile2') && $form['#entity_type'] == 'profile2') {
      if (isset($form_state['field']['#parents'])) {
        $parents = reset($form_state['field']['#parents']);
        $fields = $parents['#fields'];
      }
      // Yet an other hack, for profile2 checkboxes.
      $entity_info = entity_get_info('profile2');
      foreach ($entity_info['bundles'] as $type => $bundle) {
        foreach($form_state['values']['profile_' . $type] as $field_name => $field_data) {
          if(isset($field_data['entity_field_access']) && !empty($field_data['entity_field_access'])) {
            $form_state['values'][$field_name]['entity_field_access'] = $field_data['entity_field_access'];
          }
        }
      }
    }
    
    foreach ($fields as $field_name => $value) {
      if (isset($form_state['values'][$field_name]['entity_field_access'])) {
        $data[$field_name] = $form_state['values'][$field_name]['entity_field_access'];
      }
    }
    
    foreach ($data as $field_name => $value) {
      if(is_array($value)) {
        $new_data[$field_name] = array_filter($value);
      }
      else {
        $new_data[$field_name] = array($value => 1);
      }
    }

    return $new_data;
  }

}

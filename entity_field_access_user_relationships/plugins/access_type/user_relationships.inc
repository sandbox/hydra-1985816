<?php

$plugin = array(
  'name' => t('User relationships'),
  'description' => t('Adds access requirement per user relationship.'),
  'class' => 'entity_field_access_access_type_user_relationships',
);

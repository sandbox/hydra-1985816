<?php

/**
 * TODO: Documentation.
 */
class entity_field_access_access_type_user_relationships implements entity_field_access_access_type_interface {

  function field_settings() {
    $rel_types = user_relationships_types_load();
    foreach ($rel_types as $rel_type) {
      $rels[$rel_type->rtid] = $rel_type->name;
    }
    return $rels;
  }

  function entity_settings($field_settings, $account) {
    return $this->field_settings();
  }

  function access($entity, $entity_settings, $account) {
    // The field is being accessed by $account (the current user).
    // We need to check whether $account is permitted to see the field.
    // In other words, we check here to see if the appropriate relationship
    // exists between $account and the uid of the $entity.
    $viewer = $account->uid;
    $field_owner = $entity->uid;

    // A user can always see their own data.
    if ($viewer == $field_owner) {
      return TRUE;
    }

    // Let's see what relationships the viewer of the field has with the owner
    // of the field.
    $rel_opts = array("between" => array($viewer, $field_owner));
    $rels = user_relationships_load($rel_opts);

    // $entity_settings holds the rtid of the relationship.
    foreach ($entity_settings as $key => $value) {
      foreach ($rels as $rel) {
        if ($key == $rel->rtid) {
          if (!($rel->requires_approval)) {
            // the relationship exists, and it doesn't require approval, so we
            // allow access.
            return TRUE;
          }
          else {
            // this relationship requires approval, so only allow access if it
            // has been approved.
            if ($rel->approved) {
              return TRUE;
            }
          }
        }
      }
    }

    if (user_access('access all private fields', $account)) {
      return TRUE;
    }

    return FALSE;
  }

  function weight() {
    return 0;
  }

}

<?php

$plugin = array(
  'name' => t('Role'),
  'description' => t('Adds access requirement per role.'),
  'class' => 'entity_field_access_access_type_role',
);

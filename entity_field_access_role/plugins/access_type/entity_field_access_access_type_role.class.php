<?php

/**
 * TODO: Documentation.
 */
class entity_field_access_access_type_role implements entity_field_access_access_type_interface {

  function field_settings() {
    $role_options = array_map('check_plain', user_roles());
    return $role_options;
  }

  function entity_settings($field_settings, $account) {
    return $this->field_settings();
  }

  function access($entity, $entity_settings, $account) {
    foreach ($entity_settings as $key => $value) {
      if (array_key_exists($key, $account->roles)) {
        return TRUE;
      }
    }

    if (user_access('access all private fields', $account)) {
      return TRUE;
    }

    return FALSE;
  }

  function weight() {
    return 0;
  }

}
